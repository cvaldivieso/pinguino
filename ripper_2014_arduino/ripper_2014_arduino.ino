//naranja
#define PINGD 5
//azul
#define PINGC 6
//verde
#define PINGI 7

//cny de atras
//amarillo (principal)
#define PISOA0 22
//azul
#define PISOA1 21

//cny de adelante
//marron
#define PISOD0 0
//verde (principal)
#define PISOD1 1


//blanco-principal
#define OU1 24
//rojo
#define OU2 8
//verde-principal
#define OU3 3
//blanco
#define OU4 2

//recomendado 700
#define LIM_ULTRASONIDO 300
//minimo usable PWMP 100
#define PWMP 11
#define MAX 255
#define MIN 100

//CICLO para revisar si estaba
#define CICLO 10



void pulso(unsigned char pin) {
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW); // el digitalwrite fue cambiado por digitalWrite
  delayMicroseconds(2); //LOS DELAY COMUNES SE CAMBIARON POR ESTA FUNCION
  digitalWrite(pin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pin, LOW);

}

unsigned int duracion(unsigned char pin,unsigned int limite){
	unsigned int amplitud = 0;
	pinMode(pin,INPUT);
	//espera flanco de subida por el pin echo
	while(digitalRead(pin) == LOW){ 
		amplitud++;
		if(amplitud>64)
			break;
	}
	amplitud = 0;
	//comienza a contar centimetros hasta que pin echo sea cero
	while(digitalRead(pin) == HIGH){
	  amplitud++;
	  if (amplitud >= limite)
		return 0;
	}
	return amplitud;
}

void resetear()
{
  digitalWrite(OU1, LOW);
  digitalWrite(OU2, LOW);
  digitalWrite(OU3, LOW);
  digitalWrite(OU4, LOW);
}

void derecha() {
    digitalWrite(OU1, HIGH);
    digitalWrite(OU2, LOW);
    digitalWrite(OU3, HIGH);
    digitalWrite(OU4, LOW);
}

void izquierda() {
    digitalWrite(OU1, HIGH);
    digitalWrite(OU2, HIGH);
    digitalWrite(OU3, HIGH);
    digitalWrite(OU4, HIGH);
}

void retro() {
    digitalWrite(OU1, HIGH);
    digitalWrite(OU2, LOW);
    digitalWrite(OU3, HIGH);
    digitalWrite(OU4, HIGH);
}

void delante() {
    digitalWrite(OU1, HIGH);
    digitalWrite(OU2, HIGH);
    digitalWrite(OU3, HIGH);
    digitalWrite(OU4, LOW);
}

void velocidad(unsigned char rapidez) {
  analogWrite(PWMP, rapidez);
}

void setup()
{
  pinMode(OU1, OUTPUT);
  pinMode(OU2, OUTPUT);
  pinMode(OU3, OUTPUT);
  pinMode(OU4, OUTPUT);
  pinMode(PISOA0,INPUT);
  pinMode(PISOA1,INPUT);
  pinMode(PISOD0,INPUT);
  pinMode(PISOD1,INPUT);
  resetear();
  velocidad(MIN);
}

unsigned int dur; //duracion
unsigned int contador; //contador para verificar si el otro robot aun sigue alfrente

void loop()
{
  //sensores de piso
//  while(!(digitalRead(pisod0)==HIGH) && !(digitalRead(pisod1)==HIGH)){
  while((digitalRead(PISOD0)==LOW) && (digitalRead(PISOD1)==LOW)){
			retro();			
			velocidad(MAX);

			delay(500);}

//  while(!(digitalRead(pisoa0)==HIGH) && !(digitalRead(pisoa1)==HIGH)){
  while((digitalRead(PISOA0)==LOW) && (digitalRead(PISOA1)==LOW)){
			delante();
			velocidad(MAX);

			delay(500);}

  //sensores ultrasonido
  contador = 0;
  do {
    pulso(PINGC);
    dur = duracion(PINGC,LIM_ULTRASONIDO);    
    if (dur != 0){
      delante();
      contador++;
      //si el otro esta aun alfrente usa el turbo
      if (contador >= CICLO) {
        velocidad(MAX);
      }
    } else {
      velocidad(MIN);
    }
  } while (dur != 0);

do{
  pulso(PINGD);
  dur = duracion(PINGD,LIM_ULTRASONIDO);
  if (dur != 0) {
    izquierda();
  }
} while (dur != 0);

do{
  pulso(PINGI);
  dur = duracion(PINGI,LIM_ULTRASONIDO);

  if (dur != 0) {
    derecha();
  }
} while (dur != 0);
}
