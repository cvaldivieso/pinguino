

//

#define pinx 13
#define piny 14

//
#define pingd 5
//
#define pingc 6
//
#define pingi 7

//
//
#define pisoa0 22
//
#define pisoa1 21
//
//
#define pisod0 0
//
#define pisod1 1

//
#define ou1 24
//
#define ou2 8
//
#define ou3 3
//
#define ou4 2

//
#define lim_ultrasonido 250
//
#define pwm 11
#define MAX 925
#define MIN 825

//
#define ciclo 20

void pulso(unsigned char pin){
	pinmode(pin,OUTPUT);
	digitalwrite(pin,LOW);
	Delayms(2);
	digitalwrite(pin,HIGH);
	Delayms(5);
	digitalwrite(pin,LOW);
}
unsigned int duracion(unsigned char pin,unsigned int limite){
	unsigned int amplitud = 0;
	pinmode(pin,INPUT);
//
	while(digitalread(pin) == LOW){
		amplitud++;
		if(amplitud>64)
			break;
	}
	amplitud = 0;
//
	while(digitalread(pin) == HIGH){
	  amplitud++;
	  if (amplitud >= limite)
		return 0;
	}
	return amplitud;
}
void resetear()
{
	digitalwrite(ou1,LOW);
	digitalwrite(ou2,LOW);
	digitalwrite(ou3,LOW);
	digitalwrite(ou4,LOW);
}

void derecha(){
	digitalwrite(ou1,HIGH);
	digitalwrite(ou2,LOW);
	digitalwrite(ou3,HIGH);
	digitalwrite(ou4,LOW);	
}

void izquierda(){
	digitalwrite(ou1,HIGH);
	digitalwrite(ou2,HIGH);
	digitalwrite(ou3,HIGH);
	digitalwrite(ou4,HIGH);
}

void retro(){
	digitalwrite(ou1,HIGH);
	digitalwrite(ou2,LOW);
	digitalwrite(ou3,HIGH);
	digitalwrite(ou4,HIGH);
}

void delante(){
	digitalwrite(ou1,HIGH);
	digitalwrite(ou2,HIGH);
	digitalwrite(ou3,HIGH);
	digitalwrite(ou4,LOW);
}

void velocidad(unsigned short rapidez){
	PWM_set_dutycycle(pwm,rapidez);
}

int leerX(){
  int xRead = analogread(pinx);
  return(map(xRead, 240, 750, -500, 500));
}

int leerY(){
  int yRead = analogread(piny);
  return(map(yRead, 270, 795, -500, 500));
}

void giroIzquierda(){
  velocidad(MAX);
  izquierda();
  Delayus(400);
}

void giroDerecha(){
  velocidad(MAX);
  derecha();
  Delayus(400);
}
int actualx; //se guarda el  valor de X
int actualy; //se guarda el  valor de Y
int anteriorx; //valox anterior del acelerometro en X
int anteriory; //valox anterior del acelerometro en Y
void setup()
{
pinmode(ou1,OUTPUT);
pinmode(ou2,OUTPUT);
pinmode(ou3,OUTPUT);
pinmode(ou4,OUTPUT);
pinmode(pisoa0,INPUT);
pinmode(pisoa1,INPUT);
pinmode(pisod0,INPUT);
pinmode(pisod1,INPUT);
resetear();
velocidad(MIN);
}

unsigned int dur; //duracion
unsigned int contador; //contador para verificar si el otro robot aun sigue alfrente

void loop()
{
resetear();

//
  contador=0;
  anteriorx= leerX();
  anteriory= leerY();
do{
//

	while(!(digitalread(pisod0)==HIGH) && !(digitalread(pisod1)==HIGH)){
			retro();			
			velocidad(MAX);
			Delayms(500);}

	while(!(digitalread(pisoa0)==HIGH) && !(digitalread(pisoa1)==HIGH)){
			delante();
			velocidad(MAX);
			Delayms(500);}
			
//
	pulso(pingc);
  	dur = duracion(pingc,lim_ultrasonido);
	  if (dur != 0){
        delante();
//
        contador++;        
        if(contador>=ciclo){
            velocidad(MAX);
        }
    }
//
//
     if(actualx=leerX()<anteriorx){
//
         if(actualy=leerY()<anteriory){
           giroIzquierda();
           anteriory=actualy;
         }
//
         if(actualy=leerY()>anteriory){
           giroDerecha();
           anteriory=actualy;
         }
         anteriorx=actualx;
     }else{velocidad(MIN);}
}while(dur != 0);

do{
	pulso(pingd);
  	dur = duracion(pingd,lim_ultrasonido);
  	if (dur != 0){derecha();}
}while(dur != 0);

do{
	pulso(pingi);
  	dur = duracion(pingi,lim_ultrasonido);
  	if (dur != 0){izquierda();}
}while(dur != 0);
}

