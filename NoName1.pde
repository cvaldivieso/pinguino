//codigo para la calibracion del acelerometro
#define PIC18F4550    // Habilita el uso de los pines: 18 en adelante

//variables del acelerometro

#define pinx 13
#define piny 14

//naranga
#define pingd 5
//azul
#define pingc 6
//verde
#define pingi 7

//minimo usable pwm 300
#define pwm 11
#define MAX 925
#define MIN 825

//blanco-principal
#define ou1 24
//rojo
#define ou2 8
//verde-principal
#define ou3 3
//blanco
#define ou4 2

#define numero_iteracion 300

char arreglo[3];
int xderechamin=600;
int xderechamax=-600;
int xatrasmin=600;
int xatrasmax=-600;
int yderechamin=600;
int yderechamax=-600;
int yizqmin=600;
int yizqmax=-600;

void resetear()
{
	digitalWrite(ou1,LOW);
	digitalWrite(ou2,LOW);
	digitalWrite(ou3,LOW);
	digitalWrite(ou4,LOW);
}

void derecha(){
	digitalWrite(ou1,HIGH);
	digitalWrite(ou2,LOW);
	digitalWrite(ou3,HIGH);
	digitalWrite(ou4,LOW);	
}

void izquierda(){
	digitalWrite(ou1,HIGH);
	digitalWrite(ou2,HIGH);
	digitalWrite(ou3,HIGH);
	digitalWrite(ou4,HIGH);
}

void retro(){
	digitalWrite(ou1,HIGH);
	digitalWrite(ou2,LOW);
	digitalWrite(ou3,HIGH);
	digitalWrite(ou4,HIGH);
}

void delante(){
	digitalWrite(ou1,HIGH);
	digitalWrite(ou2,HIGH);
	digitalWrite(ou3,HIGH);
	digitalWrite(ou4,LOW);
}

void velocidad(unsigned short rapidez){
	analogWrite(pwm,rapidez);
}

int leerX(){
  int xRead = analogRead(pinx);
  return(xRead);
}

int leerY(){
  int yRead = analogRead(piny);
  return(yRead);
}

void giroIzquierda(){
  velocidad(MAX);
  izquierda();
  delay(400);
}

void giroDerecha(){
  velocidad(MAX);
  derecha();
  delay(400);
}
int numero;
void muestra(){
  numero=xderechamin;
  arreglo[0]=48+(numero/1000);
  arreglo[1]=48+((numero/100)%10);
  arreglo[2]=48+((numero/10)%10);
  arreglo[3]=48+(numero%10);
  CDC.print(arreglo,4);
  
  numero=xderechamax;
  arreglo[0]=48+(numero/1000);
  arreglo[1]=48+((numero/100)%10);
  arreglo[2]=48+((numero/10)%10);
  arreglo[3]=48+(numero%10);
  CDC.print(arreglo,4);
  
  numero=xatrasmin;
  arreglo[0]=48+(numero/1000);
  arreglo[1]=48+((numero/100)%10);
  arreglo[2]=48+((numero/10)%10);
  arreglo[3]=48+(numero%10);
  CDC.print(arreglo,4);
  
  numero=xatrasmax;
  arreglo[0]=48+(numero/1000);
  arreglo[1]=48+((numero/100)%10);
  arreglo[2]=48+((numero/10)%10);
  arreglo[3]=48+(numero%10);
  CDC.print(arreglo,4);
  
  numero=yderechamin;
  arreglo[0]=48+(numero/1000);
  arreglo[1]=48+((numero/100)%10);
  arreglo[2]=48+((numero/10)%10);
  arreglo[3]=48+(numero%10);
  CDC.print(arreglo,4);  
  
  numero=yderechamax;
  arreglo[0]=48+(numero/1000);
  arreglo[1]=48+((numero/100)%10);
  arreglo[2]=48+((numero/10)%10);
  arreglo[3]=48+(numero%10);
  CDC.print(arreglo,4);
  
  numero=yizqmin;
  arreglo[0]=48+(numero/1000);
  arreglo[1]=48+((numero/100)%10);
  arreglo[2]=48+((numero/10)%10);
  arreglo[3]=48+(numero%10);
  CDC.print(arreglo,4);
  
  numero=yizqmax;
  arreglo[0]=48+(numero/1000);
  arreglo[1]=48+((numero/100)%10);
  arreglo[2]=48+((numero/10)%10);
  arreglo[3]=48+(numero%10);
  CDC.print(arreglo,4);
  
  muestra();
}

int actualx; //se guarda el  valor de X
int actualy; //se guarda el  valor de Y
int anteriorx; //valox anterior del acelerometro en X
int anteriory; //valox anterior del acelerometro en Y

void setup()
{
pinMode(ou1,OUTPUT);
pinMode(ou2,OUTPUT);
pinMode(ou3,OUTPUT);
pinMode(ou4,OUTPUT);

resetear();
velocidad(MIN);
}

unsigned int contador; //contador para verificar si el otro robot aun sigue alfrente
int i;
void loop()
{
resetear();

  //va adelante
delante();
velocidad(1023);
for (i=0;i<numero_iteracion;i++){
  delante();
  anteriorx=leerX();
  if(anteriorx<xderechamin){
	xderechamin=anteriorx;}
  if(anteriorx>xderechamax){
	xderechamax=anteriorx;}
}
retro();
velocidad(1023);
for (i=0;i<numero_iteracion;i++){
  delante();
  anteriorx=leerX();
  if(anteriorx<xatrasmin){
	xatrasmin=anteriorx;}
  if(anteriorx>xatrasmax){
	xatrasmax=anteriorx;}
}
derecha();
velocidad(1023);
for (i=0;i<numero_iteracion;i++){
  anteriory= leerY();
  if(anteriory<yderechamin){
	yderechamin=anteriory;}
  if(anteriorx>yderechamax){
	yderechamax=anteriory;}
}
izquierda();
velocidad(1023);
for (i=0;i<numero_iteracion;i++){
  anteriory= leerY();
  if(anteriory<yizqmin){
	yizqmin=anteriory;}
  if(anteriorx>yizqmax){
	yizqmax=anteriory;}
}
muestra();
}