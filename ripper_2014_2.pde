#define PIC18F4550    // Habilita el uso de los pines: 18 en adelante

#define psharp 20
//limita la distancia del sharp
#define limite_sharp 250

//naranga
#define pingd 5
//azul
#define pingc 6
//verde
#define pingi 7

//cny de atras
//amarillo (principal)
#define pisoa0 22
//azul
#define pisoa1 21
//cny de adelante
//marron
#define pisod0 0
//verde (primario)
#define pisod1 1


//blanco-principal
#define ou1 24
//rojo
#define ou2 8
//verde-principal
#define ou3 3
//blanco
#define ou4 2

//recomendado 700
//limita la distancia del ultrasonido
#define lim_ultrasonido 250
//minimo usable pwm 300
#define pwm 11
#define MAX 925
#define MIN 850

//ciclo para revisar si estaba
#define ciclo 20

void pulso(unsigned char pin){
	pinMode(pin,OUTPUT);
	digitalwrite(pin,LOW);
	delay(2);
	digitalwrite(pin,HIGH);
	delay(5);
	digitalwrite(pin,LOW);
}
unsigned int duracion(unsigned char pin,unsigned int limite){
	unsigned int amplitud = 0;
	pinMode(pin,INPUT);
	//espera flanco de subida por el pin echo
	while(digitalRead(pin) == LOW){
		amplitud++;
		if(amplitud>64)
			break;
	}
	amplitud = 0;
	//comienza a contar centimetros hasta que pin echo sea cero
	while(digitalread(pin) == HIGH){
	  amplitud++;
	  if (amplitud >= limite)
		return 0;
	}
	return amplitud;
}
void resetear()
{
	digitalWrite(ou1,LOW);
	digitalWrite(ou2,LOW);
	digitalWrite(ou3,LOW);
	digitalWrite(ou4,LOW);
}

void derecha(){
	digitalWrite(ou1,HIGH);
	digitalWrite(ou2,LOW);
	digitalWrite(ou3,HIGH);
	digitalWrite(ou4,LOW);	
}

void izquierda(){
	digitalWrite(ou1,HIGH);
	digitalWrite(ou2,HIGH);
	digitalWrite(ou3,HIGH);
	digitalWrite(ou4,HIGH);
}

void retro(){
	digitalWrite(ou1,HIGH);
	digitalWrite(ou2,LOW);
	digitalWrite(ou3,HIGH);
	digitalWrite(ou4,HIGH);
}

void delante(){
	digitalWrite(ou1,HIGH);
	digitalWrite(ou2,HIGH);
	digitalWrite(ou3,HIGH);
	digitalWrite(ou4,LOW);
}

void velocidad(unsigned short rapidez){
	analogWrite(pwm,rapidez);
}

void setup()
{
pinMode(ou1,OUTPUT);
pinMode(ou2,OUTPUT);
pinMode(ou3,OUTPUT);
pinMode(ou4,OUTPUT);
pinMode(pisoa0,INPUT);
pinMode(pisoa1,INPUT);
pinMode(pisod0,INPUT);
pinMode(pisod1,INPUT);
resetear();
velocidad(MIN);
}

unsigned int dur; //duracion
unsigned int contador; //contador para verificar si el otro robot aun sigue alfrente
int sharp=0;
void loop()
{
resetear();
contador=0;

sharp=analogRead(psharp);
while(sharp>limite_sharp){
	retro();
	velocidad(MAX);
}
//sensores ultrasonido
do{
	//sensores de piso

/*	while(!(digitalRead(pisod0)==HIGH) && !(digitalRead(pisod1)==HIGH)){
			retro();			
			velocidad(MAX);
			delay(700);}

	while(!(digitalRead(pisoa0)==HIGH) && !(digitalRead(pisoa1)==HIGH)){
			delante();
			velocidad(MAX);
			delay(700);}
*/
//sensores ultrasonido
	pulso(pingc);
  	dur = duracion(pingc,lim_ultrasonido);
	  if (dur != 0){
        delante();
		//turbo si lo tiene delante
        contador++;        
        if(contador>=ciclo){
            velocidad(MAX);
        }
    }else{velocidad(MIN);}
}while(dur != 0);
do{
	pulso(pingd);
  	dur = duracion(pingd,lim_ultrasonido);
  	if (dur != 0){derecha();}
}while(dur != 0);

do{
	pulso(pingi);
  	dur = duracion(pingi,lim_ultrasonido);
  	if (dur != 0){izquierda();}
}while(dur != 0);
}