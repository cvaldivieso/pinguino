#define PIC18F4550    // Habilita el uso de los pines: 18 en adelante
#define ou1 24
#define ou2 8
#define ou3 3
#define ou4 2
#define pwm 11

void resetear()
{
	digitalWrite(ou1,LOW);
	digitalWrite(ou2,LOW);
	digitalWrite(ou3,LOW);
	digitalWrite(ou4,LOW);
}

void delante(){
	digitalWrite(ou1,HIGH);
	digitalWrite(ou2,HIGH);
	digitalWrite(ou3,HIGH);
	digitalWrite(ou4,LOW);
}

void retro(){
	digitalWrite(ou1,HIGH);
	digitalWrite(ou2,LOW);
	digitalWrite(ou3,HIGH);
	digitalWrite(ou4,HIGH);
}

void setup()
{
pinMode(ou1,OUTPUT);
pinMode(ou2,OUTPUT);
pinMode(ou3,OUTPUT);
pinMode(ou4,OUTPUT);
resetear();
}
int i=0;
void loop()
{
delante();
for (i=1023;i>500;i=i-15){
	analogWrite(pwm,i);
	delay(300);
}
for (i=225;i<1023;i=i+15){	
	analogWrite(pwm,i);
	delay(300);
}
resetear();
delay(500);
	retro();
for (i=225;i<1023;i=i+15){	
	analogWrite(pwm,i);
	delay(300);
}
for (i=1023;i>225;i=i-15){
	analogWrite(pwm,i);
	delay(300);
}
resetear();
delay(500);
}